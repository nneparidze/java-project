module UBGarden.student.main {

    requires javafx.graphics;
    requires javafx.controls;
    requires javafx.fxml;

    opens fr.ubx.poo.ugarden;
}